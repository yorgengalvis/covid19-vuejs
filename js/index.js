const app = new Vue({
    el:'#app',

    created() {
        this.getTotalCasos();
        this.getDatos();
        this.getCiudades();
        this.getTotalAtencion();
        this.getCiudadVisitante();
    },

    data:{
        titulo:'COVID19 EN COLOMBIA',
        tiempo:0,
        valor_dolar: null,
        fecha : new Date().toISOString().substr(0,10),
        fecha_maxima : new Date().toISOString().substr(0,10),
        ciudad:'',
        ip:0,
        totalCasos:0,
        numeroCasos:0,
        ciudades:[],
        numeroRecuperados:0,
        numeroCasa:0,
        numeroHospitales:0,
        numeroHospitalesU:0,
        numeroFallecidos:0,
        fechaActual:"02/20/2020"
    },



    methods: {
        getDatos(){

            let ciudad = this.ciudad;
            const url_api = `https://www.datos.gov.co/resource/gt2j-8ykr.json?ciudad_de_ubicaci_n=${ciudad}`;
            try {
                this.$http.get(url_api).then((response)=>{
                    this.numeroCasos = response.data.length;            
                });
            } catch (error) {
               console.log(err)
           }
       },


       getTotalCasos(){
        var f = new Date();
        this.fechaActual=f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();

        const url_api = `https://www.datos.gov.co/resource/gt2j-8ykr.json`;
        try {
         this.$http.get(url_api).then((response)=>{

          this.totalCasos = response.data.length;            
      });
     } catch (error) {
        console.log(err)
    }
},

getCiudades(){
    let ciudades = []; 
    let url="https://www.datos.gov.co/resource/gt2j-8ykr.json";
    try{
        this.$http.get(url).then((response)=>{
            for (var i = 0; i < response.data.length; i++) {
                ciudades[i] = response.data[i].ciudad_de_ubicaci_n;
                //console.log(response.data[i].ciudad_de_ubicaci_n);
           }  
           this.ciudades = new Set(ciudades);
       });
    }catch(error){
        console.log(error)
    }

    for (let item of this.ciudades) console.log(item);

},

getCiudadVisitante(){
    const urlip = 'https://api.ipify.org/?format=json'; //Obtener IP
    try{
        this.$http.get(urlip).then((response)=>{
           const url_apiIP='http://geoplugin.net/json.gp?ip='+response.data.ip;
           this.$http.get(url_apiIP).then((responsee)=>{
            this.ciudad=responsee.data.geoplugin_city;
           });
        });
    }catch(error){
        console.log(error);
    }
},

getTotalAtencion(){
    this.getAtencion('Recuperado');
    this.getAtencion('Casa');
    this.getAtencion('Hospital');
    this.getAtencion('Hospital UCI');
    this.getAtencion('Fallecido');
},

getAtencion(atencion){
    const url_api = `https://www.datos.gov.co/resource/gt2j-8ykr.json`;
    try {
        this.$http.get(url_api).then((response)=>{
            var numeroCasoss=0;
            for(let i=0; i< response.data.length; i++){
                if(response.data[i].atenci_n === atencion){
                    numeroCasoss+=1;
                }
            }    
            if(atencion==="Recuperado") this.numeroRecuperados=numeroCasoss;
            if(atencion==="Casa") this.numeroCasa=numeroCasoss;
            if(atencion==="Hospital") this.numeroHospitales=numeroCasoss;
            if(atencion==="Hospital UCI") this.numeroHospitalesU=numeroCasoss;
            if(atencion==="Fallecido") this.numeroFallecidos=numeroCasoss;
        });
    } catch (error) {
       console.log(err)
   }
}



},

})
